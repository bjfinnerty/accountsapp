
var ExportsWrapper = function(){

    var _this = this;

    this.AddExpenseForm = Backbone.View.extend({

        template: appTemplates.newExpense,
        el:$('#newExpense'),

        initialize: function(){
            this.model.on("change:price", this.changeVat);
            this.render();
        },

        events:{
            "click a#save": "saveExpense",
            "keyup input#price": "changeVat"
        },

        render: function(){
            cl('render new expense', this.model);
            //this.model.set("vat", Math.round(this.model.get("price")*23)/100);
            //this.model.set("date", this.model.get("date").toString('d/m/yyyy'));
            var tpl = _.template(this.template);
            this.$el.html(tpl(this.model.toJSON()));
            return this;
        },

        changeVat:function(e){
            var price = parseInt($(e.target).val());
            if(!isNaN(price)){
                var vat = (price * 23)/100;
                //this.model.set("vat", vat);
                $("#vat").val(vat);
            }
        },

        saveExpense:function(e){
            e.preventDefault();
            cl('firing repeatedly');
            var self = this;

           // this.model = new appModels.Expense();
            var formVals = utils.parseQuerystring($("#expense-entry").serialize());
            formVals.dateString = decodeURIComponent(formVals.date);//Date.parseExact(decodeURIComponent(formVals.date), 'd/m/yyyy');
            formVals.date = utils.dateFromString(formVals.dateString);
            formVals.name = decodeURIComponent(formVals.name);
            formVals.price = parseInt(formVals.price*100)/100;
            formVals.vat = parseInt(formVals.price * 23)/100;


            this.model.save(formVals,{
                success: function(){

                //    cl('succes', self.collection);
                    self.collection.add(self.model);
                   // self.collection.update(self.model);
                    self.model = new appModels.Expense();
                    self.render();
                }
            });
           // cl(this.model);
            //this.collection.create(this.model);

        }

    });

    this.ExpenseView = Backbone.View.extend({

        tagName: "tr",
        className: "singleExpense",
        template: appTemplates.singleExpense,

        attributes:{
            //where is the model?
        },

        render: function(){
            this.model.set("vat", Math.round(this.model.get("price")*23)/100);
            this.model.set("date", this.model.get("date").toString('d/m/yyyy'));
            var tpl = _.template(this.template);
            this.$el.attr("id", this.model.get("_id"));
            this.$el.html(tpl(this.model.toJSON()));
            return this;
        }

    });

    this.ExpenseSheetView = Backbone.View.extend({

        el: $("#expenses"),
        addEl:$("#newExpense"),
        aggTemplate: appTemplates.aggExpenses,


        initialize: function(){


            if(!this.collection || this.collection == undefined){
                this.collection = new appModels.ExpenseSheet();
            }

            if(this.options.month){
                var startDate = new Date(this.options.month+" 1, 2012");
                this.collection.view.query.startkey = new Date((startDate.getMonth()+2)+" 0, 2012");
                this.collection.view.query.endkey = startDate;

                cl(this.collection.view.query)
            }

            this.collection.fetch({
                success: function(model, response){
                    //model.render();
                }
            }, this);

            this.collection.on("reset", this.render, this);
            this.collection.on("add", this.render, this);
            this.collection.on("remove", this.render, this);
            this.collection.bind("update", this.render, this);
            this.delegateEvents();
        },

        events:{
            "click a.edit":"editExpense",
            "click a.delete":"deleteExpense"
        },

        render:function(){
            console.log('render expense sheet');
            this.$el.find("#expenseRows tr").remove();
            var that = this;

            _.each(this.collection.models, function(expense){
                that.renderExpense(expense);
            }, this);

            this.renderAggregates(this.collection.models)
        },

        renderExpense: function(expense){
            var expenseView = new _this.ExpenseView({
                model:expense, collection:this.collection
            });

            this.$el.find('#expenseRows').append(expenseView.render().el)
        },


        renderAggregates: function(coll){
            var agg = {};
            agg.totalPrice = utils.addObjVals(this.collection.toJSON(), "price");
            agg.totalVat = Math.round(agg.totalPrice*23)/100;
            cl(this.collection.toJSON());
            var tpl = _.template(this.aggTemplate);
            this.$el.find('#expenseTotals').html(tpl(agg));
        },

        editExpense: function(e){
            // this refers to the view

            var targetId = $(e.target).data("model-id");
            var targetModel = this.collection.get(targetId);
            var targetNode = $("#"+targetId);
            addExpenseForm.model = targetModel;
            addExpenseForm.render();
        },

        deleteExpense: function(e){
            var targetId = $(e.target).data("model-id");
            var targetModel = this.collection.get(targetId);
            targetModel.destroy();
            cl(this.collection);
            //this.collection.remove(targetModel)
        }

    });
};


// Wrap all the models in a closure, then run through each method and assign them the the exports global object.
// This makes it easier to reference local functions and objects

var exportWrapper = new ExportsWrapper();
$.each(exportWrapper , function(key, val ){
    exports[key] = val;
});