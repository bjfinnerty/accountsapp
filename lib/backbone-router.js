
// no need for exports wrapper as only using one router for this app

exports.AppRouter = Backbone.Router.extend({

    routes:{
        "expenses/:monthName/": "expenseView",
        "expenses/": "expenseView",
        "timesheet/:monthName/":"timesheetView",
        "timesheet/":"timesheetView",
        "invoice/:month/": "invoiceView",
        "invoice/": "invoiceView"
    },

    timesheetView:function(monthName){
        console.log('timesheetView', monthName);

        //window.monthView = new MonthView({collection:daysFromDb(monthName)});
        // directory.filterType = contactType;
        // directory.trigger("change:filterType");
    },

    expenseView:function(monthName){

        console.log('app router: expenseView', monthName);

        if(monthName == undefined){

            var expenseSheet = new appViews.ExpenseSheetView({collection:new appModels.ExpenseSheet()});
            window.addExpenseForm = new appViews.AddExpenseForm({model:new appModels.Expense(), collection:expenseSheet.collection});

        } else{
            var expenseSheet = new appViews.ExpenseSheetView({collection:new appModels.ExpenseSheet(), month:monthName});
            window.addExpenseForm = new appViews.AddExpenseForm({model:new appModels.Expense(), collection:expenseSheet.collection});
        }



        //window.monthView = new MonthView({collection:daysFromDb(monthName)});
        // directory.filterType = contactType;
        // directory.trigger("change:filterType");
    },

    invoiceView:function(monthName){

        console.log('invoiceView', monthName);

        //window.monthView = new MonthView({collection:daysFromDb(monthName)});
        // directory.filterType = contactType;
        // directory.trigger("change:filterType");
    }
});




