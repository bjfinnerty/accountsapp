
var ExportsWrapper = function(){

    var _this = this;

    this.Expense = Backbone.Model.extend({

        idAttribute: "_id",

        defaults:{
            type:"expense",
            name:"Add new expense...",
            price: 0,
            vat: 0,
            dateString: utils.dateToString(new Date()),
            date:new Date()
        },

        initialize: function(){
            this.bind('destroy', this.removeFromCollection)
        },

        removeFromCollection:function(){
            cl(this);
        },

        validate: function(attrs){
            _.each(attrs, function(val, key, obj){
                switch(key){
                    case "id":
                        if(val == ""){
                            return "invalid Id"
                        }
                        break;
                    case "type":
                        if(val != "expense"){
                            return "invalid Type"
                        }
                        break;
                    case "name":
                        if(val == "Add New Expense..." || val == ""){
                            return "Check Expense"
                        }
                        break;
                    case "price":
                        if(isNaN(val) || val == 0){
                            return "Price isnt number or is 0"
                        }
                        break;
                    case "vat":
                        if(isNaN(val) || val == 0){
                            return "Vat isnt number"
                        }
                        break;
                    case "date":
                        if(!val || val.constructor != Date){
                            return "Invalid Date"
                        }
                        break;

                }
            });

        }
    });

    this.ExpenseSheet = Backbone.Collection.extend({

        model: this.Expense,

      /*  parse: function(data){
           cl('parse', data, this);
        },*/


        list:{
            name:"collectionFormat"
        },

        view: {
            ddoc: 'accounts',
            name: 'allExpenses',
            query: {
                descending:true
            }
        }
    });

};

var exportWrapper = new ExportsWrapper();
$.each(exportWrapper , function(key, val ){
    exports[key] = val;
});