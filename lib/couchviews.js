/**
 * Created with PyCharm.
 * User: brianfinnerty
 * Date: 23/08/2012
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */


exports.views = {
    allExpenses:{
        map:function(doc){
            if (doc.type == "expense") {
                emit(doc.date, doc);
            }
        }


    }

};

exports.shows = {
    summary:function(doc, req){
        return "<h1>" + doc.type + "</h1>"
    }
};

exports.lists = {

    collectionFormat:function(head, req){
        var row;
        while(row = getRow()) {
            log(row.key);
            send(toJSON(row.value));
        }
    }

};