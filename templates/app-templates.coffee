exports.tablerow="""
  <tr><td>Test</td></tr>
"""

exports.singleExpense = """
  <td><%= name %></td>
  <td><%= price %></td>
  <td><%= vat %></td>
  <td><%= dateString %></td>
  <td style="width:40px"><a class="btn btn-warning btn-small edit" data-model-id="<%= _id %>">Edit</a></td>
  <td style="width:40px"><a class="btn btn-danger btn-small delete" data-model-id="<%= _id %>">Delete</a></td>
  """

exports.newExpense = """

  <h4>Add New...</h4>

  <form class="form-horizontal" id="expense-entry">
    <div class="control-group">
        <label class="control-label" for="name">Name</label><input value="<%= name %>" id="name" name="name">
    </div>
    <div class="control-group">
        <label class="control-label" for="price">Price</label><input value="<%= price %>" id="price" name="price">
    </div>
    <div class="control-group">
        <label class="control-label" for="vat">Vat</label><input value="<%= vat %>" id="vat" readonly="" name="vat">
    </div>
    <div class="control-group">
        <label class="control-label" for="date">Date</label><input data-date-format="dd/mm/yyyy" value="<%= dateString %>" id="date" name="date">
    </div>
    <% try {if(!_rev || _rev != undefined){ %>
      <input id="revision" type="hidden" name="_rev" id="_rev" value="<%= _rev %>"/>
    <% }}catch(err){}%>
  </form>
  <a id="save" class="btn btn-large btn-success">Add New Expense</a>


"""

exports.aggExpenses = """

    <tr class="totals">
        <th>&nbsp;</th>
        <th>Total Price</th>
        <th>Total Vat</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <td><%= totalPrice %></td>
        <td><%= totalVat %></td>
        <th>&nbsp;</th>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>

"""